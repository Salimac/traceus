import { AppRegistry } from 'react-native';
import boot from "./artifacts/boot/index";
const app = boot();
AppRegistry.registerComponent('starter', () => app);