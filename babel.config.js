module.exports = {
  presets: ['module:metro-react-native-babel-preset'],
  plugins: [
    [
      'module-resolver',
      {
        extensions: ['.ts', '.tsx', '.json'],
        alias: {
          '@Components': './src/components',
          '@Screens': './src/screens',
          '@Modules': './src/modules',
          '@Assets': './src/assets/index',
        },
      },
    ],
  ],
};
