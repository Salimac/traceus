const assets: any = {
    common: {
      blur: function(){return require('./blur.jpeg')},
      loader: function(){return require('./loader.gif')},
      profile: function(){return require('./profile.jpg')},
      gift: function(){return require('./gift.png')},
      glob: function(){return require('./glob.png')},
      home: function(){return require('./home.png')},
      invoice: function(){return require('./invoice.png')},
      category: function(){return require('./category.png')},
      cup: function(){return require('./cup.png')},
      tabBg: function(){return require('./navBg.png')},
      dots: function(){return require('./dots.png')}
    },
  };
  
  export default assets;
  