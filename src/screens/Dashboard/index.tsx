import React, { Fragment } from 'react';
import {
  Dimensions,
  View,
  SafeAreaView,
  KeyboardAvoidingView,
  Platform,
  TouchableOpacity,
  FlatList
} from 'react-native';
import { Button, Text, Content, Icon } from 'native-base';

import { connect } from 'react-redux';
import styled from 'styled-components';

const SButton = styled(Button)`
  bottom: 10;
  left: 0;
  border-radius: 25;
  height: 50;
  justify-content: center;
  width: 100%;
`;

const ContentWrapper = styled(KeyboardAvoidingView)`
  background-color: transparent;
  margin-top: 0;
  margin-bottom: 0;
  flex: 1;
  padding: 0;
`;

const SContent = styled(Content)`
  padding: 20px 0;
  flex: 1;
  background-color: transparent;
`;

export interface Props {
  navigation?: any;
  user?: any;
  devices?: any;
}

class DashboardComponent extends React.Component<Props, any> {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const { devices } = this.props;

    console.log({ devices });
    return (
      <SafeAreaView>
        <ContentWrapper behavior="padding">
          <SContent
            contentContainerStyle={{
              flex: 1,
              justifyContent: 'space-between'
            }}
          >
            <Fragment>
              <FlatList
                style={{ flex: 1 }}
                data={devices}
                contentContainerStyle={{ padding: 10 }}
                renderItem={({item, index}: any) => {
                  const {id,manufacturerData} = item || {} as any;
                  return (
                    <View>
                      <Text>{`${index} - id: ${id} - ${manufacturerData}`}</Text>
                    </View>
                  )
                }}
              />
            </Fragment>
          </SContent>
        </ContentWrapper>
      </SafeAreaView>
    );
  }
}

const mapDispatchToProps = {};

const mapPropstoProps = state => ({
  user: state.common.user,
  devices: state.common.signals
});

const Dashboard = connect(
  mapPropstoProps,
  mapDispatchToProps
)(DashboardComponent);

export { Dashboard };
