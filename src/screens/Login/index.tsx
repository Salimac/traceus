import React from 'react';
import {
  Dimensions,
  View,
  SafeAreaView,
  KeyboardAvoidingView,
} from 'react-native';
import { Button, Text, Content } from 'native-base';
import { CustomInput } from '@Components/Inputs';
import { reduxForm, Field, change, reset } from 'redux-form';
import { connect } from 'react-redux';
import styled from 'styled-components';
import { setUser, loadUser } from '@Modules/Common';
import { required } from '@Components/Inputs/validation';
import isEmpty from 'lodash/isEmpty';

const requiredMobile = required('Mobile');

const { width: deviceWidth, height: deviceHeight } = Dimensions.get('window');

const SView = styled(View)`
  width: ${deviceWidth};
  height: ${deviceHeight};
`;

const SButton = styled(Button)`
  position: absolute;
  bottom: 10;
  left: 0;
  border-radius: 25;
  height: 50;
  justify-content: center;
  width: 100%;
`;

const ContentWrapper = styled(KeyboardAvoidingView)`
  background-color: transparent;
  margin-top: 0;
  margin-bottom: 0;
  flex: 1;
  padding: 0;
`;

const SContent = styled(Content)`
  padding: 20px;
  flex: 1;
  background-color: transparent;
`;

const TopView = styled(View)`
  flex: 1;
`;
export interface Props {
  navigation?: any;
  handleSubmit: Function;
  changeFieldValue: Function;
  resetForm: Function;
  setUser: Function;
  loadUser: Function;
  login?: any;
  user?: any;
}

class LoginFrom extends React.Component<Props, {}> {
  constructor(props) {
    super(props);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  componentDidMount() {
    this.props.loadUser();
  }

  handleSubmit() {
    const { login, navigation, resetForm, setUser } =
      this.props || ({} as any);
    const { values } = login || ({} as any);
    const { mobile } = values || ({} as any);

    if (!mobile) {
      let userObj = {
        mobile
      };
      setUser(userObj);
    } else {
      setUser(null);
    }

    resetForm('login');
    navigation.navigate('Dashboard');
  }

  render() {
    const inputStyle = {
      borderRadius: 25,
      height: 50,
      backgroundColor: '#eee',
      flexDirection: 'row',
      justifyContent: 'flex-start'
    };
    const { login, user } = this.props || ({} as any);
    const { mobile: mo } = user || ({} as any);
    const inValid = login ? !isEmpty(login.syncErrors) : true;

    let props = {} as any;
    if (inValid) {
      props.warning = true;
      props.onPress = () => {};
    } else {
      props.primary = true;
    }

    const styleProps = {
      style: {
        //   backgroundColor: '#00adef'
      }
    };
    return (
      <SafeAreaView {...styleProps}>
        <SView>
          <ContentWrapper behavior="padding">
            <SContent
              contentContainerStyle={{
                flex: 1,
                justifyContent: 'space-between'
              }}
            >
              <TopView>
                <Field
                  label={'Mobile'}
                  name="mobile"
                  component={CustomInput}
                  placeholder={"+601XXXXXXxXX"}
                  inputStyle={inputStyle}
                  validate={[requiredMobile]}
                  defaultValue={mo}
                />
              </TopView>
              {
                //@ts-ignore
                <SButton onPress={this.handleSubmit} {...props}>
                  <Text> Login </Text>
                </SButton>
              }
            </SContent>
          </ContentWrapper>
        </SView>
      </SafeAreaView>
    );
  }
}

const LoginContainer = reduxForm({
  destroyOnUnmount: false,
  enableReinitialize: true,
  keepDirtyOnReinitialize: true,
  form: 'login'
})(LoginFrom);

const mapDispatchToProps = {
  changeFieldValue: change,
  resetForm: reset,
  setUser,
  loadUser
};

const mapPropstoProps = state => ({
  login: state.form.login,
  initialValues: state.common.user,
  user: state.common.user
});

const Login = connect(
  mapPropstoProps,
  mapDispatchToProps
)(LoginContainer);

export { Login };
