import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';
import commonReducer from '@Modules/Common/';

export default combineReducers({
  common: commonReducer,
  form: formReducer
});
