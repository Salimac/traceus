import React, { Fragment } from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  BackHandler,
  AppState,
  Platform
} from 'react-native';

import { connect } from 'react-redux';

import styled from 'styled-components';

import { SideBar } from '@Components/SideBar';
import { Login } from '@Screens/Login';
import { Dashboard } from '@Screens/Dashboard';
import SplashScreen from 'react-native-splash-screen';
import { Wrapper } from '@Components/Wrapper';
import { Bluetooth } from "./util";

const SScrollView = styled(ScrollView)`
  background-color: white;
`;


export interface State {
  Screen?: JSX.Element;
  ScreenObj?: any;
  params?: any;
  stackObj?: any;
}

const Screens = [
  {
    name : 'Login',
    Component : Login
  },
  {
    name : 'Dashboard',
    Component : Dashboard,
    header: true,
    footer: true
  },
]
class App extends React.Component<any,State>{
  backHandler;
  
  constructor(props){
    const dScreen = Screens[0];
    const { name, Component } = dScreen || {} as any;
    super(props);
    this.state = {
      params: {},
      stackObj: [name],
      ScreenObj : dScreen,
      Screen: Component
    };
    this.navigate = this.navigate.bind(this)
    this.handleBack = this.handleBack.bind(this)
    this.handleAppStateChange = this.handleAppStateChange.bind(this)
    if (__DEV__) {
      console.disableYellowBox = true;
    }
  }

  handleBack(){
    const { ScreenObj } = this.state;
    const { name, Component } = ScreenObj || {} as any;

    if(name === 'Dashboard' || name === 'Login'){
      BackHandler.exitApp();
      return false;
    } else {
      this.navigate(null, {action : 'goBack'})
      return true;
    }
  }

  handleAppStateChange(nextAppState) {
    if (nextAppState.match(/inactive|background/)) {

      // AppState.removeEventListener('change', this.handleAppStateChange);
    }
  }

  componentDidMount(){
    SplashScreen.hide();
    this.backHandler = BackHandler.addEventListener('hardwareBackPress',this.handleBack);

    AppState.addEventListener('change', this.handleAppStateChange);

    const BluetoothObj = new Bluetooth();
    BluetoothObj.init();
  }

  componentWillUnmount(){
    this.backHandler.remove()
  }
  
  navigate(trg,nav){
    let stackObj = this.state.stackObj;
    const length = stackObj.length;
    const { action, ...params } = nav || {} as any;
  
    let target = trg;
    
    if(action === "goBack"){
      if(length > 1){
        target = stackObj.pop();
        target = stackObj.slice(-1)[0];
      }
    } 
  
    const screen = Screens.find(screen => screen.name === target) || Screens[0];
    const { Component, name } = screen || {} as any;

    stackObj = stackObj.filter(scr=> scr !== name);
    stackObj = [...stackObj,name];
    this.setState({ Screen : Component, params: {...params }, ScreenObj: screen, stackObj });
  }

  render(){
    SplashScreen.hide();
    //@ts-ignore
    const { Screen, params, ScreenObj } = this.state || {} as any;;
    const { header, footer, name } = ScreenObj || {} as any;
    const paramWrapper = {
      ...params, 
      navigation: {
        navigate : this.navigate,
        goBack : ()=>this.navigate(null,{ action: 'goBack' })
    }};
    
    return (
      <Fragment>
        <StatusBar backgroundColor="#00adef" barStyle="light-content" />
        <SafeAreaView  style={{ backgroundColor: '#00adef'}}>
        <SideBar {...paramWrapper}/>
        <Wrapper header={header} footer={footer} navigation={paramWrapper.navigation} screen={name}>
            <SScrollView contentInsetAdjustmentBehavior="automatic">
              {
                //@ts-ignore
                <Screen {...paramWrapper}/>
              }
            </SScrollView>
          </Wrapper>
        </SafeAreaView>
      </Fragment>
    );
  }
};

const mapDispatchToProps = null;

const mapPropstoProps = state => ({
  transactions: state.common.transactions
});

const AppContainer =  connect(
    mapPropstoProps,
    mapDispatchToProps
)(App);

export default AppContainer;
