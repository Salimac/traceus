import update from 'immutability-helper';
import isEmpty from 'lodash/isEmpty';
import AsyncStorage from '@react-native-community/async-storage';
const TOGGLE = 'common/TOGGLE';
const LOAD_TRANSACTIONS = 'common/LOAD_TRANSACTIONS';
const ADD_TRANSACTION = 'common/ADD_TRANSACTION';

const LOAD_MERCHANT_TRANSACTIONS = 'common/LOAD_MERCHANT_TRANSACTIONS';
const ADD_MERCHANT_TRANSACTION = 'common/ADD_MERCHANT_TRANSACTION';
const UPDATE_USER = 'common/UPDATE_USER';
const TOGGLE_SIDEBAR = 'common/TOGGLE_SIDEBAR';
const UPDATE_SIGNALS = 'common/UPDATE_SIGNALS';

export function updateSignals(signals){
  return {
    payload : signals,
    type: UPDATE_SIGNALS
  };
}

function handleUpdateSignals(state,action){
  return update(state, {
    signals: {
      $set: action.payload
    },
  });
}

export function toggleSideBar(show?){
  return {
    payload : !!show,
    type: TOGGLE_SIDEBAR,
  };
}

function handleToggleSideBar(state,action){
  return update(state, {
    show: {
      $set: action.payload,
    },
  });
}

export function loadUser(){
  return async function(dispatch){
    const res = await AsyncStorage.getItem('user') as any;
    let user =  JSON.parse(res) || {};
    if(!!user){
      let { balance } = user || {} as any;
      if(!balance) {
        user = {
          ...user,
          balance : 1100
        }
      }
    }
    dispatch({  
      payload: user,
      type: UPDATE_USER,
    });
    return user;
  }
}

export function loadMerchantTransactions () {
  return dispatch => {
    AsyncStorage.getItem('merchantTransactions').then(res=>{
      let trans =  res;
      if(!!trans){
        trans = JSON.parse(trans);
      }
      dispatch({  
        payload: trans || [],
        type: LOAD_MERCHANT_TRANSACTIONS,
      });
    })
  }
}

function handleLoadMerchantTransactions(state, action) {
  return update(state, {
    merchantTransactions: {
      $set: action.payload,
    },
  });
}

export function addMerchantTransactions(payload) {
  return dispatch => {
    dispatch({  
      payload: payload,
      type: ADD_MERCHANT_TRANSACTION,
    });
  }
}

function handleAddMerchantTransaction(state, action) {
  let trans = [...state.merchantTransactions,...[{
    ...action.payload,
    date: new Date()
  }]];

  AsyncStorage.setItem('merchantTransactions', JSON.stringify(trans));
  return update(state, {
    merchantTransactions: {
      $set: trans,
    },
  });
}

export function loadTransactions() {
  return dispatch => {
    AsyncStorage.getItem('userTransactions').then(res=>{
      let trans =  res;
      if(!!trans){
        trans = JSON.parse(trans);
      }
      dispatch({  
        payload: trans || [],
        type: LOAD_TRANSACTIONS,
      });
    })
  }
}

function handleLoadTransactions(state, action) {
  return update(state, {
    transactions: {
      $set: action.payload,
    },
  });
}

export function addTransactions(payload) {
  return dispatch => {
    dispatch({  
      payload: payload,
      type: ADD_TRANSACTION,
    });
  }
}

function handleAddTransaction(state, action) {
  let trans = [...state.transactions,...[{
    ...action.payload,
    date: new Date()
  }]];

  let user = state.user;
  let { amount } = action.payload || {amount:'0.00'};
  amount = parseFloat(amount.replace(/,/gi, ''));

  const { balance } = user || {balance : 0};

  user = {
    ...user,
    balance : balance - amount
  }

  AsyncStorage.setItem('userTransactions', JSON.stringify(trans));
  AsyncStorage.setItem('user', JSON.stringify(user));
  return update(state, {
    transactions: {
      $set: trans,
    },
    user : {
      $set: user
    }
  });
}

export function updateToggle(payload) {
  return {
    payload,
    type: TOGGLE,
  };
}

function handleUpdateToggle(state, action) {
  return update(state, {
    toggle: {
      $set: action.payload,
    },
  });
}

export function setUser(usr){
  if(!!usr){
    AsyncStorage.setItem('user',JSON.stringify(usr));
  };
  return {
    payload: usr,
    type: UPDATE_USER,
  };
}

function handleUpdateUser(state,action) {
  return update(state, {
    user: {
      $set: action.payload,
    },
  });
}


//this is the dummy
export function transaction(payload){
  return dispatch => {
    dispatch(addTransactions(payload))
    dispatch(addMerchantTransactions(payload))
  }
}

export const ACTION_HANDLERS = {
  [TOGGLE]: handleUpdateToggle,
  [LOAD_TRANSACTIONS]: handleLoadTransactions,
  [ADD_TRANSACTION]: handleAddTransaction,
  [LOAD_MERCHANT_TRANSACTIONS]: handleLoadMerchantTransactions,
  [ADD_MERCHANT_TRANSACTION]: handleAddMerchantTransaction,
  [UPDATE_USER] : handleUpdateUser,
  [TOGGLE_SIDEBAR]: handleToggleSideBar,
  [UPDATE_SIGNALS]: handleUpdateSignals,
};

const initialState = {
  toggle: false,
  transactions: [],
  merchantTransactions: [],
  user: null,
  merchantBalance: 4100,
  show: false,
  signals: null
};

export default function reducer(state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type];
  return handler ? handler(state, action) : state;
}
