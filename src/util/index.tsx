/* eslint-disable @typescript-eslint/no-unused-vars */
import { BleManager } from "react-native-ble-plx";
import { store } from '../boot/setup';
import { updateSignals } from '../modules/Common';
import { request, PERMISSIONS } from 'react-native-permissions';
import { Platform } from 'react-native';

export const dispatch = func => {
  store && store.dispatch(func);
};

export class Bluetooth {
  manager = {} as any;
  state = null;
  subscription = null as any;
  public init() {

    const permissionType = Platform.OS === 'android' ? 
    PERMISSIONS.ANDROID.ACCESS_COARSE_LOCATION : 
    PERMISSIONS.IOS.BLUETOOTH_PERIPHERAL;

    request(permissionType).then(result => {
      console.log({result})
    });


    this.manager = new BleManager() as any;
    this.subscription = this.manager.onStateChange(st => {

      console.log({state: st})

      this.state = st;
      if (st === 'PoweredOn') {
        this.scanAndConnect();
        this.subscription.remove();
      }
    }, true);
    
  }

  public setupNotifications(device) {
    console.log({ notificationTo: device });
  }

  public scanAndConnect() {
    let devices = !!store && !!store.getState().common && store.getState().common.signals;
    devices = devices || [];

    this.manager.startDeviceScan(null, null, (error, device) => {
      //pushe the new scanned device
      devices = devices.filter(dv => dv.id !== device.id);
      devices.push(device);


      store.dispatch(updateSignals(devices));

      if (error) {
        console.log({ error });
        return;
      }

      //for further action like stop scanning or connect to pair specific device
      if (device.name && false) {
        this.manager.stopDeviceScan();
        device
          .connect()
          .then(device => {
            console.log("Discovering services and characteristics");
            return device.discoverAllServicesAndCharacteristics();
          })
          .then(device => {
            console.log("Setting notifications");
            return this.setupNotifications(device);
          })
          .then(
            () => {
              console.log("Listening...");
            },
            error => {
              console.log(error.message);
            }
          );
      }
    });
  }
}


export default {};
