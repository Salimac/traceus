import React from 'react';
import {
  Platform,
  TextInput,
  Dimensions,
  View,
  Text
} from 'react-native';

import { Item, Input, Label, Switch } from 'native-base';
import styled from 'styled-components';

const { width: deviceWidth } = Dimensions.get('window');


const ItemView = styled(View)`
  margin-bottom: 10px;
`;

const ErrorText = styled(Text)`
  color: red;
  font-size: 12px;
  padding-top: 3px;
  padding-left: 2px;
  border-bottom-width: 0;
  background-color: transparent;
  margin-left: 13;
`;

const SBoldLabel = styled(Label)`
    font-weight: bold;
    font-size: 13;
    margin-bottom : 2;
    margin-left: 13;
    color: #3e3e3e;
`;


class AmountInput extends React.PureComponent<any, {}> {
  oldText;
  input;
  input2;
  value;
  sizeMap;
  props: any;
  constructor(props) {
    super(props);
    this.oldText = '0.00';
    this.value = 0;
    this.input = null;
    this.input2 = null;
    this.handleOnChange = this.handleOnChange.bind(this);
    this.changeValTo = this.changeValTo.bind(this);
    this.setFocus = this.setFocus.bind(this);
    this.setLayout = this.setLayout.bind(this);
  }

  setFocus() {
    setTimeout(() => {
      this.input.focus();
      // this.input.setNativeProps({
      //   selection: { start: this.oldText.length, end: this.oldText.length },
      // });
    }, 0);
  }

  setLayout(ev) {
    const { nativeEvent: en } = ev || ({} as any);
    const { layout: ly } = en || ({} as any);
    const { x, y, width, height } = ly || ({} as any);
    setTimeout(() => {
              // selection: { start: this.oldText.length, end: this.oldText.length },
      this.input.setNativeProps({
        width
      });
    }, 0);
  }

  formatAmout(tx) {
    let num = (parseFloat(tx) / 100) as any;
    num = num.toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
    num = num.length < 4 ? '0.00' : num;
    return num;
  }

  componentDidMount() {
    try {
      const { input } = this.props || ({} as any);
      const { value } = input || ({} as any);
      this.oldText = parseFloat(value) > 0 ? value : '0.00';
      this.changeValTo(this.oldText);
    } catch (er) {}
  }

  changeValTo(txt) {
    const { input } = this.props || ({} as any);
    setTimeout(() => {
      this.input.setNativeProps({ text: txt });
    }, 0);
    setTimeout(() => {
      this.input2.setNativeProps({ text: txt });
    }, 0);

    input.onChange(txt);
  }

  handleOnChange(ev) {
    const text = ev.nativeEvent.text;
    let newText = text
      .replace(/,/gi, '')
      .replace('.', '')
      .replace(/\D+/g, '');
    newText = newText.replace(/\D+/g, '');

    let oldVal = parseInt(!!newText ? newText : '0');
    let tx = this.formatAmout(oldVal);

    if (!!oldVal && oldVal.toString().length > 9) {
      oldVal = parseFloat(oldVal.toString().substr(0, 9));
      tx = this.formatAmout(oldVal);
      this.changeValTo(tx);
      return;
    }

    newText = tx;
    this.oldText = tx;
    this.changeValTo(tx);
  }

  render() {
    const {
      meta,
      preLabel,
      label,
      postIcon,
      totalStyle,
      inputStyle,
      input,
      ...otherProps
    } = this.props || ({} as any);

    const { name, value } = input || ({} as any);

    let totalProps =
      Platform.OS === 'android'
        ? {
            ...otherProps,
            underlineColorAndroid: 'transparent',
            name,
            value,
          }
        : {
            ...otherProps,
            name,
            value,
          };

    const isError = meta && !!meta.error; //&& meta.touched;
    
    return (
      <ItemView>
        {label && <SBoldLabel>{label}</SBoldLabel>}
        <View style={{
            ...inputStyle,
            borderColor : isError ? 'red' : inputStyle.borderColor,
            borderWidth : isError ? 2 : 1
          }}>
          {preLabel && (
             <Input
              editable={false}
              style={{
                color: '#323232',
                paddingRight: 0,
                position: 'absolute',
                top: 0,
                left: 15,
                zIndex : 3,
                paddingLeft: 0,
                fontSize: 23,
                fontWeight: 'bold',
                height: 48,
                width: 20
              }}
            >
              {preLabel}
            </Input>
          )}
          <View style={{ flex: 0.8, left: 35 }}>
            <TextInput
              ref={ref => (this.input = ref)}
              style={{
                color: 'transparent',
                flex: 1,
                position: 'absolute',
                top: 0,
                paddingRight: 0,
                paddingLeft: 0,
                fontSize: 23,
                fontWeight: 'bold',
                height: 48,
                width: 50,
                borderWidth: 0,
                textAlign: 'right',
                zIndex: 3
              }}
              defaultValue="0.00"
              onChange={this.handleOnChange}
              placeholderTextColor="transparent"
              {...totalProps}
            />
            <TextInput
              onFocus={this.setFocus}
              ref={ref => (this.input2 = ref)}
              style={{
                position: 'absolute',
                top: 0,
                right: 0,
                flex: 1,
                left: 0,
                zIndex: 1,
                color: '#323232',
                paddingRight: 0,
                paddingLeft: 0,
                fontSize: 23,
                height: 48,
                width: '100%',
                fontWeight: 'bold',
                borderWidth: 0,
              }}
              defaultValue="0.00"
              {...otherProps}
              placeholderTextColor="#3232324C"
            />
            <Text
              style={{
                position: 'absolute',
                top: 0,
                flex: 1,
                left: 0,
                zIndex: 0,
                color: 'transparent',
                paddingRight: 0,
                paddingLeft: 0,
                fontSize: 23,
                fontWeight: 'bold',
                height: 48,
              }}
              onLayout={this.setLayout}
            >
              {value}
            </Text>
          </View>
          {!!postIcon && postIcon}
        </View>
        {isError && <ErrorText>{meta.error}</ErrorText>}
      </ItemView>
    );
  }
}

//Start Additional Renderer
class CustomInput extends React.PureComponent<any, {}> {

  UNSAFE_componentWillReceiveProps(nProps){
    const {input, defaultValue } = this.props;
    let { value } = input || {} as any;
    value = !!defaultValue ? defaultValue : value;

    const {input:inp, defaultValue: df } = nProps;
    let { value:vl } = inp || {} as any;
    vl = !!df ? df : vl;

    if(!value && value!== vl){
      input.onChange(vl)
    }
  }
  render(){
    const {
      input,
      name,
      meta,
      label,
      onChange,
      validateBefore,
      defaultValue,
      autoFocus,
      keyboardType,
      returnKeyType,
      password,
      ...otherProps
    } = this.props;

    const isError = meta.error && (validateBefore ? true : meta.touched);
    let newProps = {
      ...otherProps,
      autoFocus: !!autoFocus ? true : false,
      keyboardType: !!keyboardType ? keyboardType : 'default',
      returnKeyType: !!returnKeyType ? returnKeyType : 'done',
      underlineColorAndroid: Platform.OS === 'android' ? 'transparent' : undefined
    } as any;
  
    const inputStyle = {
      borderRadius: 25,
      height: 50,
      backgroundColor: '#eee',
      borderColor: '#cecece',
      padding: 0
    } as any;
  
    
    return (
      <ItemView>
        {label && <SBoldLabel>{label}</SBoldLabel>}
        <View style={{
          ...inputStyle,
          borderColor : isError ? 'red' : inputStyle.borderColor,
          borderWidth : isError ? 2 : 1
        }}>
            <TextInput
              style={{
                color: '#323232',
                paddingRight: 10,
                paddingLeft: 10,
                borderColor: 'transparent',
                fontSize: 15,
                height: 50,
              }}
              placeholderTextColor="#3232324C"
              {...input}
              {...newProps}
              autoFocus = {!!newProps ? !!newProps.value : false}
              value={!!newProps ? newProps.value : ''}
              defaultValue={defaultValue}
              secureTextEntry={!!password ? true : false}
            />
        </View>
        {isError && <ErrorText>{meta && meta.error}</ErrorText>}
      </ItemView>
    );
  }
};

//Start Additional Renderer
const SwitchComponent = ({
  input,
  label
}) => {

  const { name, value } = input || ({} as any);
  return (
    <View style={{flexDirection: 'row', justifyContent: 'space-between', marginTop: 10}}>
      {label && <SBoldLabel>{label}</SBoldLabel>}
        <Switch
          onValueChange= {val=>input.onChange(val)}
          {...input}
        />
    </View>
  );
};

export {
  AmountInput,
  CustomInput,
  SwitchComponent as Switch
}