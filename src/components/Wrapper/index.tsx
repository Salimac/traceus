import React from 'react';
import {
  Dimensions,
  View,
  TouchableOpacity,
  Image,
  Text,
  Animated
} from 'react-native';
import { Icon } from 'native-base';

import { connect } from 'react-redux';
import styled from 'styled-components';
import { toggleSideBar } from '@Modules/Common';
import Assets from '@Assets';

const { width: deviceWidth, height: deviceHeight } = Dimensions.get('window');

const SView = styled(View)`
  width: ${deviceWidth};
  height: 100%;
`;

const SButton = styled(TouchableOpacity)`
  height: 70;
  justify-content: center;
  align-items: center;
`;

const TButton = styled(TouchableOpacity)`
  height: 50;
  width: 70;
  align-items: center;
  justify-content: center;
`;

const HeaderView = styled(View)`
  background-color: #00adef;
  top: 0;
  left: 0;
  height: 50;
  width: ${deviceWidth};
  flex-direction: row;
  justify-content: flex-start;
`;

const TitleView = styled(View)`
  background-color: #00adef;
  height: 50;
  width: ${deviceWidth - 140};
  flex-direction: row;
  justify-content: center;
`;

const Title = styled(Text)`
  font-family: "Philosopher-BoldItalic";
  height: 50;
  line-height: 50;
  font-size: 20;
  color: white;
`;

const FooterView = styled(View)`
  position: absolute;
  bottom: 0;
  left: 0;
  height: 70;
  width: ${deviceWidth};
  z-index: 10;
  border-top-width: 1;
  border-top-color: #cfcfcf;
  flex-direction: row;
  justify-content: flex-start;
  background-color: white;
`;

export interface Props {
  navigation?: any;
  toggleSideBar: Function;
  children?: JSX.Element;
  header?: boolean;
  footer?: boolean;
  user?: any;
  screen?: string;
}

class WrapperComponent extends React.Component<Props, any> {
  animatedValue;
  viewInterpolate;
  val;
  constructor(props) {
    super(props);
    this.animatedValue = new Animated.Value(0);
    this.val = 0;
    const radio = deviceWidth / 72;

    this.viewInterpolate = [
      {
        translateY: this.animatedValue.interpolate({
          inputRange: [0, 1],
          outputRange: [0, ((18 * radio) / 4 + 50) * -1]
        })
      },
      {
        scaleX: this.animatedValue.interpolate({
          inputRange: [0, 1],
          outputRange: [1, radio]
        })
      },
      {
        scaleY: this.animatedValue.interpolate({
          inputRange: [0, 1],
          outputRange: [1, radio]
        }),
      },
    ];
  }

  expand(va) {
    Animated.spring(this.animatedValue, {
      toValue: va === 1 ? 0 : 1,
      velocity: 1,
      friction: 3,
      useNativeDriver: true
    }).start();
    this.val = va === 1 ? 0 : 1;
  }

  handleSubmit() {
    const { navigation } = this.props || ({} as any);
  }

  render() {
    const { navigation, header, footer, children, user, screen } = this.props;
    let height = deviceHeight - (header ? 50 : 0);
    height = height - (footer ? 60 : 0);

    const footerObj = [
      {
        icon: Assets.common.home(),
        name: 'Dashboard',
        onPress: () => navigation.navigate('Dashboard')
      },
    ];

    const centerGap = (deviceWidth * 350) / 1521;
    const wrapperWidth = (deviceWidth - centerGap) / 2;

    return (
      <SView
        style={{
          width: deviceWidth,
        }}
      >
        {!!header && (
          <HeaderView>
            <TButton onPress={() => this.props.toggleSideBar(true)}>
              <Icon style={{ fontSize: 30, color: 'white' }} name="ios-menu" />
            </TButton>
            <TitleView>
              <Title>Trace Us</Title>
            </TitleView>
          </HeaderView>
        )}
        <SView style={{ height }}>{children}</SView>
        {!!footer && (
          <FooterView>
            <View
              style={{
                width: wrapperWidth,
                height: 70,
                flexDirection: 'row'
              }}
            >
              {footerObj.map((obj, index) => {
                return (
                  <SButton
                    key={index}
                    onPress={obj.onPress}
                    style={{
                      width: wrapperWidth / 2
                    }}
                  >
                    {
                      //@ts-ignore
                      <Image
                        resizeMode="contain"
                        style={{
                          width: 24,
                          height: 24,
                          opacity: obj.name === screen ? 1 : 0.4,
                        }}
                        source={obj.icon}
                      />
                    }
                  </SButton>
                );
              })}
            </View>
          </FooterView>
        )}
      </SView>
    );
  }
}

const mapDispatchToProps = {
  toggleSideBar
};

const mapPropstoProps = state => ({
  user: state.common.user
});

const Wrapper = connect(
  mapPropstoProps,
  mapDispatchToProps
)(WrapperComponent);

export { Wrapper };
