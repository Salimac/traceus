import React, { Fragment } from 'react';
import { Dimensions,
  View,
  SafeAreaView, 
  KeyboardAvoidingView,
  Platform,
  TouchableWithoutFeedback,
  Animated,
  Image,
  ImageBackground
} from 'react-native';
import { Button, Text, Content, ListItem } from 'native-base';
import { toggleSideBar } from '@Modules/Common';
import Assets from '@Assets';

import { connect } from 'react-redux';
import styled from 'styled-components';
import { userInfo } from 'os';

const { width : windowWidth } = Dimensions.get('window') as any;


const ProfileImage = styled(Image)`
  flex: 1;
  width: 100%;
  height: 100%;
  align-items: center;
  justify-content: center;
`;

const ImageView = styled.View `
  width: 60;
  height: 60;
  border-radius: 30;
  border-color: white;
  border-width: 3;
  overflow: hidden;
  justify-content: center;
  align-items: center;
`;

const SView = styled(Animated.View)`
  flex-direction: row;
  justify-content: flex-start;
  position: absolute;
  z-index: 99;
  /* top: 0; */
  left: 0;
  bottom: 0;
  height: 100%;
  /* border-width:5;
  border-color:red; */
  width: ${windowWidth * 0.68};
  background-color: transparent;
`;

const Backdrop = styled(Animated.View)`
  position: absolute;
  z-index: 98;
  /* top: 0; */
  left: 0;
  bottom: 0;
  opacity: 0;
  width: 100%;
  height: 100%;
  background-color: black;
`;

const SButton = styled(Button)`
    position: absolute;
    bottom: 10;
    left: 0;
    border-radius: 25;
    height: 50;
    justify-content: center;
    width: 100%;
`;

const TButton = styled(SButton)`
    bottom: 70;
`;

const ContentWrapper = styled(KeyboardAvoidingView)`
  background-color: transparent;
  margin-top: 0;
  margin-bottom: 0;
  flex: 1;
  padding: 0;
`;

const SContent = styled(Content)`
  flex: 1;
  background-color: transparent;
`;

const TopShadow = styled(View)`
  height: 300;
  opacity: 0.6;
  position: absolute;
  top:0;
  left: 0;
  background-color: black;
  width : ${windowWidth * 0.68};
`;

const TopView = styled(View)`
  height: 240;
  padding: 20px 10px 0px;
  width : ${windowWidth * 0.68};
`;

const BottomView = styled(View)`
  flex: 1;
  height: 100%;
  padding: 0px 10px;
  background-color: #00adef;;
  width : ${windowWidth * 0.68};
`;

const Title = styled(Text)`
  font-family: 'SpecialElite-Regular';
  height: 30;
  line-height:30;
  font-size: 20;
  color: white;
`;

export interface Props {
  navigation?: any;
  user?: any;
}


class SideBarComponent extends React.Component<any,any>{

    xPosition;

    constructor(props){
        super(props);
        this.xPosition = new Animated.Value(0);
        this.state = {}
    }
    
    componentWillReceiveProps(nProps){
        const { show: visible } = this.props || {} as any;
        const { show } = nProps;
        if(show !== visible){
            Animated.spring(this.xPosition, {
                toValue: show ? 1 : 0
            }).start()
        }
    }

    componentDidMount(){

      const data = [{
        lable: "Option One",
        route: ()=>{}
      },{
        lable: "Option Two",
        route: ()=>{}
      },{
        lable: "Option Three",
        route: ()=>{}
      },{
        lable: "Option Four",
        route: ()=>{}
      }]
      this.setState({data})
    }

    render(){
      const SideBarProps = {
        style : {
          flex: 1,
          overflow: 'hidden',
          width: windowWidth * 0.68,
          height: '100%',
          backgroundColor: 'white',
          // borderWidth: 1,
          // borderColor: 'black',
          // borderStyle : 'dotted',
          borderBottomRightRadius : 15,
          borderTopRightRadius: 15,
        },
        forceInset: { top: 'always', bottom: 'always' },
      } as any;

      const { show, user } = this.props || {} as any;
      const { email } = user || {} as any;
      const { data } = this.state || {} as any;
      return show && (
          <Fragment>
                <TouchableWithoutFeedback onPress={()=>{
                    Animated.spring(this.xPosition, {
                      toValue: 0
                    }).start(()=>{
                      this.props.toggleSideBar()
                    })   
                  }}>
                  <Backdrop 
                    style={{
                      opacity: this.xPosition.interpolate({
                          inputRange: [0, 1],
                          outputRange: [0, 0.6]
                        }),
                    }}
                  />
                </TouchableWithoutFeedback>
                <SView
                  style={{
                      transform: [{
                          translateX: this.xPosition.interpolate({
                            inputRange: [0, 1],
                            outputRange: [-1 * windowWidth, 0]
                          }),
                      }],
                  }}
                >
                    <SafeAreaView {...SideBarProps}>
                    <ImageBackground
                      source={Assets.common.blur()}
                      style={{
                        flex: 1,
                        width: '100%',
                        height: '100%',
                        alignItems: 'center',
                        justifyContent: 'center'
                      }}
                    >
                        <ContentWrapper  behavior="padding">
                            <SContent contentContainerStyle= {{
                            flex: 1,
                            justifyContent: 'space-between'
                            }}>
                              <TopShadow />
                              <TopView>
                                <Fragment>
                                    <ImageView>
      
                                        <ProfileImage source={Assets.common.profile()} />

                                    </ImageView>
                                    <Text style={{ color:'white', fontSize: 13, fontWeight: 'bold', marginTop: 10 }}>{email}</Text>
                                </Fragment>
                              </TopView>
                              <BottomView>
                                    {
                                      data.map((opt,dx)=>{
                                        return <ListItem key={dx}><Title>{opt.lable}</Title></ListItem>
                                      })
                                    }
                                    <TouchableWithoutFeedback onPress={()=>{
                                      this.props.toggleSideBar()
                                      this.props.navigation.navigate('Login')
                                    }}>
                                        {
                                          //@ts-ignore
                                          <Text style={{position: 'absolute', bottom: 20,left:20 , color:'white', fontSize: 15, fontWeight: 'bold' }}>LogOut</Text>
                                        }
                                    </TouchableWithoutFeedback>
                              </BottomView>
                            </SContent>
                        </ContentWrapper>
                    </ImageBackground>
                    </SafeAreaView>
                </SView>
          </Fragment>
      );
    }
}

const mapDispatchToProps = {
    toggleSideBar
};

const mapPropstoProps = state => ({
  show: state.common.show,
  user: state.common.user
});

const SideBar =  connect(
    mapPropstoProps,
    mapDispatchToProps
)(SideBarComponent);

export { SideBar };

